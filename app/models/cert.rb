class Cert < ApplicationRecord
    has_many :emissions
    has_one_attached :front_image
    has_one_attached :back_image
end

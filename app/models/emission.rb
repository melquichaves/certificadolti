class Emission < ApplicationRecord
    belongs_to :cert
    has_one_attached :certpdf
end

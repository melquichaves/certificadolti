class CertlistController < ApplicationController
    def index
        session[:user_id] = params.require :user_id
        user_id = session[:user_id]

        @user_emissions = Emission.where(user_id: user_id).all
    end
end

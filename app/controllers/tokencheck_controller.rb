class TokencheckController < ApplicationController
    def show
        @validation_token = params[:validation_token]
        user_emissions = Emission.where "validation_token like ?", "#{@validation_token}"

        if user_emissions.empty?
            @status = "danger"
            @message = "Código não encontrado!"
        else
            @status = "success"
            @message = "Código encontrado!"
            @emission = user_emissions.first
        end
    end
end

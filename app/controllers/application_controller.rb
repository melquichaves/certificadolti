# required for LTI
require 'ims/lti'
# Used to validate oauth signatures
require 'oauth/request_proxy/action_controller_request'
# Gerar token do certificado
require 'securerandom'



class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  # CSRF stuff ^

  # LTI Launch endpoint (responds to POST and GET - see routes.rb)
  # checks for required query params
  # checks that the oauth signature checks out
  # read more about IMS::LTI in ims-lti gem https://github.com/instructure/ims-lti
  # note the oauth nonce is not handled within ims-lti gem, it's up to you
  def launch
    provider = IMS::LTI::ToolProvider.new(
      Rails.application.config.lti_settings['consumer_key'],
      Rails.application.config.lti_settings['consumer_secret'],
      request.query_parameters
    )

    if not provider.valid_request?(request)
      # the request wasnt validated :(
      render :launch_error, status: 401
      return
    end

    # The providre request is valid
    # store the values you need from the LTI
    # here we're just tossing them into the session
    session[:user_id] = params.require :user_id
    session[:context_id] = params.require :context_id
    session[:context_title] = params.require :context_title
    session[:lis_person_name_full] = params.require :lis_person_name_full
    session[:roles] = params.require :roles
    session[:resource_link_title] = params.require :resource_link_title
    
    #informações enviadas pelo canvas
    resource = session[:resource_link_title]
    context = session[:context_id]
    roles = session[:roles]
    user_id = session[:user_id]
    fullname = session[:lis_person_name_full]
    context_title = session[:context_title]

    if resource == "Listar certificados"
      redirect_to controller: "/certlist", user_id: user_id
      return
    end
    if resource == "Validar Certificado"
      redirect_to controller: "/tokencheck"
      return
    end 


    cert = Cert.where contex_id: context
    
    if cert.empty? #Caso não exista informação de certificado no banco
      if roles == "urn:lti:instrole:ims/lis/Administrator" #checar se regra é válida pra qualquer admin
        @course_id = context 
        @context_title = context_title
        @cert = Cert.new
        @verbose = "Criar"
        render new_cert_path
      else
        # TODO - Pagina avisando usuário que certificado não foi criado
        status_cert = "n encontrou"
      end
    else #Se existir informação do certificado no banco
      cert_context = Cert.where(contex_id: context).first

      # pegando horas do certificado e convertendo pra string
      #hours = Cert.where(:contex_id => context).pluck(:hours).first
      hours_string = cert_context.hours.to_s
      textprincipal = cert_context.description.to_s
      textmenu = cert_context.menu.to_s
      textname = cert_context.name.to_s

      #regra para substituir tags de texto
      if textprincipal =~ /\[nome\]/
        textprincipal.gsub!(/\[nome\]/, "<b style='text-decoration: underline'>"+fullname+"</b>")
      end

      if textprincipal =~ /\[curso\]/
        textprincipal.gsub!(/\[curso\]/, "<b style='text-decoration: underline'>"+context_title+"</b>")
      end

      if textprincipal =~ /\[horas\]/
        textprincipal.gsub!(/\[horas\]/, "<b style='text-decoration: underline'>"+hours_string+" Horas</b>")
      end

      # texto do usuario formatado
      @formated_name = textname
      @formated_description = textprincipal
      @formated_menu = textmenu

      # diretorio do arquivo PDF
      save_path = Rails.root.join('public',user_id+context+'.pdf')


      # Salvando no banco se não existir informação
      # Dessa forma é evitado que existam registros duplicados para o mesmo curso e
      # Permite que o usuário sobreescreva o arquivo por uma versão de Cert atualizada
      # Contudo um novo certificado é gerado sempre que a página é carregada
      # Talvez isso consuma recursos em demasia do servidor
      # Checar com o cliente a melhor abordagem posteriormente
      has_record = Emission.joins(:cert).where(:user_id => user_id).where(:certs => {:contex_id => context})
      if has_record.empty?
        #gerar token
        cert_token = SecureRandom.hex(6)

        emissao = Emission.create(user_id: user_id, cert_location: save_path, validation_token: cert_token)
        emissao.cert = cert_context
        emissao.save
      else
        cert_token = has_record.first.validation_token
      end

      # Usar token antigo se atualizar
      @token_show = cert_token

      # carregar imagens
      @front_image = wicked_active_storage_asset(cert_context.front_image)
      @back_image = wicked_active_storage_asset(cert_context.back_image)

      # Procura registro de emissão usuário atual
      # Gera String para criar PDF
      # Salva Arquivo PDF com active storage
      emission_record = Emission.joins(:cert).where(:user_id => user_id).where(:certs => {:contex_id => context})
      pdf = render_to_string pdf: "certificado", template: "application/cert_template.html.erb", encoding: "UTF-8", orientation: 'Landscape', margin:  {top: 2, bottom: 2,left: 2,right: 2}
      emission_record.first.certpdf.attach(io: StringIO.new(pdf), filename: user_id+context+".pdf", content_type: "application/pdf")
      @pdf_record = emission_record.first
      @cert_record = cert_context
      @roles = roles
  
    end
  end

  # Encontra o diretório da imagem para 
  # renderizar arquivos do Active Storage 
  # dentro do wiked_pdf
  def wicked_active_storage_asset(asset)
    return unless asset.respond_to?(:blob)
    save_path = Rails.root.join('tmp', asset.id.to_s)
    File.open(save_path, 'wb') do |file|
      file << asset.blob.download
    end
    save_path.to_s
  end

  # lTI XML Configuration
  # Used for easily installing your LTI into an LMS
  def lti_config
    render template: "application/lti_config.xml"
  end
  

end

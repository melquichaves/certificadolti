class CertsController < ApplicationController
    def create
        cert = Cert.create!(create_params)
        render :launch_created
    end

    def edit
        @cert = Cert.find(params[:id])
        @verbose = "Atualizar"
        render :new
    end

    def update
       id = params[:id]
       @cert = Cert.find(id)
       update_records = params.require(:cert).permit(:name, :description, :menu, :hours, :front_image, :back_image)

       if @cert.update update_records
            render :launch_created
       else
            flash[:notice] = "Ocorreu um erro na atualização, tente novamente."
            render :new
       end
    end    
    
    private
    def create_params
        params.require(:cert).permit(:name, :description, :menu, :hours, :front_image, :back_image, :contex_id, :context_title)
    end
end

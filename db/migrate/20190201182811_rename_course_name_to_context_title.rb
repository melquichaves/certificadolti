class RenameCourseNameToContextTitle < ActiveRecord::Migration[5.2]
  def change
    rename_column :certs, :course_name, :context_name
  end
end

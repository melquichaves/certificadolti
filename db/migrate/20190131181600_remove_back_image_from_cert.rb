class RemoveBackImageFromCert < ActiveRecord::Migration[5.2]
  def change
    remove_column :certs, :back_image, :text
  end
end

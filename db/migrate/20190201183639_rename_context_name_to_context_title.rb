class RenameContextNameToContextTitle < ActiveRecord::Migration[5.2]
  def change
    rename_column :certs, :context_name, :context_title
  end
end

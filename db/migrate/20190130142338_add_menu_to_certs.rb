class AddMenuToCerts < ActiveRecord::Migration[5.0]
  def change
    add_column :certs, :menu, :text
  end
end

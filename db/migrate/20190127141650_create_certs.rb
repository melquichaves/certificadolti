class CreateCerts < ActiveRecord::Migration[5.0]
  def change
    create_table :certs do |t|
      t.string :name
      t.text :description
      t.integer :hours
      t.string :front_image
      t.string :back_image
      t.string :contex_id

      t.timestamps
    end
  end
end

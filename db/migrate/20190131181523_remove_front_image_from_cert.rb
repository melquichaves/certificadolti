class RemoveFrontImageFromCert < ActiveRecord::Migration[5.2]
  def change
    remove_column :certs, :front_image, :text
  end
end

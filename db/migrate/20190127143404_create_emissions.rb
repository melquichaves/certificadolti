class CreateEmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :emissions do |t|
      t.integer :cert_id
      t.string :user_id
      t.string :cert_location
      t.string :validation_token

      t.timestamps
    end
  end
end

class AddCourseNameToCerts < ActiveRecord::Migration[5.2]
  def change
    add_column :certs, :course_name, :string
  end
end

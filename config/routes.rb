Rails.application.routes.draw do
  root 'application#launch'

  resources :certs, only: [:new, :create, :edit, :update]
  resources :tokencheck, only: [:show, :index]
  get 'tokencheck/index', to: "tokencheck#index"
  get 'tokencheck/check', to: "tokencheck#check"
  get 'application/launch_created', to: "application#launch_created"
  get 'application/redirect', to: "application#redirect"
  
  get 'certlist', to: "certlist#index"
  
  
  mount PdfjsViewer::Rails::Engine => "/pdfjs", as: 'pdfjs'
  
  # LTI XML CONFIG
  get :lti_config, controller: :application, as: :lti_config

  # LTI LAUNCH URL (responds to get and post)
  match 'launch' => 'application#launch', via: [:get, :post], as: :lti_launch
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
